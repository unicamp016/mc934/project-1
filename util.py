import functools
import glob
import os
import json

import tensorflow as tf
from tensorboard.plugins.hparams import api as hp

import numpy as np

SEED = 123
CONTENT_DIR = "data"
SPLITS = ["train", "val", "test"]
LABELS = ["benign", "malignant"]
LABEL_TO_INT = {
    "benign": 0,
    "malignant": 1,
}
HP_PARAMS = [
    hp.HParam("num_layers", hp.Discrete([3, 5])),
    hp.HParam("activation", hp.Discrete(["relu", "elu"])),
    hp.HParam("initializer", hp.Discrete(["glorot_uniform", "he_uniform"])),
    hp.HParam("dropout", hp.Discrete([False, True])),
]


def get_dataset_dir(split):
  assert split in SPLITS, "Invalid split name."
  return os.path.join(CONTENT_DIR, f"isic2017-{split}")


@functools.lru_cache(maxsize=128)
def get_samples(split, label):
  assert label in LABELS, "Invalid class label name."
  return glob.glob(os.path.join(get_dataset_dir(split), label, "*.png"))


def get_log_dir(path):
  return os.path.join("logs", path)


def get_ckpt_dir(path):
  return os.path.join("ckpts", path)

def get_model(path):
  return os.path.join("ckpts", path)

def get_hparam_by_name(name):
  for hparam in HP_PARAMS:
    if name == hparam.name:
      return hparam
  return None

def get_history(model_name):
  history_path = os.path.join(get_log_dir(model_name), 'history.npy')
  return np.load(history_path, allow_pickle=True).item()

def get_best_model(model_name):
  model_path = os.path.join(get_ckpt_dir(model_name), 'best_model.hdf5')
  return tf.keras.models.load_model(model_path)

def get_config_json(model_name):
  j = ''
  f = os.path.join(get_log_dir(model_name), 'config.json')
  for line in open(f, 'r'):
    if line.strip() == '': break
    j += line

  return json.loads(j)

def get_train_config(hparams, name):
  ckpt_callback = tf.keras.callbacks.ModelCheckpoint(
      os.path.join(get_ckpt_dir(name), "best_model.hdf5"),
      monitor="val_auc",
      verbose=1,
      save_best_only=True,
      mode="max",
      save_weights_only=False)
  tensorboard_callback = tf.keras.callbacks.TensorBoard(
      get_log_dir(name), histogram_freq=1)
  callbacks = [tensorboard_callback, ckpt_callback]
  if hparams:
    hparams_callback_dict = {}
    for param_name, param_value in hparams.items():
      hparam_obj = get_hparam_by_name(param_name)
      hparams_callback_dict[hparam_obj] = param_value
    hparams_callback = hp.KerasCallback(
        get_log_dir(name), hparams_callback_dict)
    callbacks.append(hparams_callback)
  return {
      "epochs": 100,
      "callbacks": callbacks,
  }
