import os

import numpy as np
import tensorflow as tf
import tensorflow.keras as keras

from util import *

BUFFER_SIZE = 512
BATCH_SIZE = 64
IMAGE_SIZE = (128, 128)
TOTAL = 2000
POS = 0.2 * TOTAL
NEG = 0.8 * TOTAL
AUGMENT_DATAGEN = keras.preprocessing.image.ImageDataGenerator(
    # featurewise_center=True,
    rotation_range=270,
    zoom_range=0.2,
    width_shift_range=0.1,
    height_shift_range=0.1,
)


def img_file_to_sample(img_file, label):
  """Convert an image file and the label to an input sample (x, y)."""
  img_data = tf.io.read_file(img_file)
  img = tf.io.decode_png(img_data)
  img = tf.image.resize(img, IMAGE_SIZE, method=tf.image.ResizeMethod.BILINEAR)
  return tf.cast(img, tf.float32), label


def get_dataset_for_label(split, label):
  """Get a Dataset object for the given label.

  Args:
    split: String with one of {'train', 'val', 'test'}.
    label: String with class label of {'benign', 'malignant'}.

  Returns:
    A Dataset object that yields an (image, label) sample, where image is a
    float32 tensor with 3 channels and label is an integer that is 0 for benign
    and 1 for malignant.
  """
  assert label in LABELS, f"Unrecognized label {label}."
  dataset_dir = os.path.join(get_dataset_dir(split), label, "*")
  files_ds = tf.data.Dataset.list_files(dataset_dir).shuffle(
      BUFFER_SIZE, seed=SEED)
  labeled_ds = files_ds.map(
      lambda f: img_file_to_sample(f, LABEL_TO_INT[label]))
  return labeled_ds


def eager_map(func):

  def wrapper(images, labels):
    return tf.py_function(
        func, inp=(images, labels), Tout=(images.dtype, labels.dtype))

  return wrapper


@eager_map
def apply_augment(images, labels):
  """Apply augmentation defined by `datagen` to the batch.

  Args:
    images: A tensor batch of images.
    labels: A tensor batch of labels.

  Return:
    A tuple with an image and label batch of the same size with
    random transformations applied to it.
  """
  return np.array(
      list(
          map(lambda img: AUGMENT_DATAGEN.random_transform(img, seed=SEED),
              images.numpy()))), labels


def inceptionv3_preprocess(images, labels):
  """Apply preprocessing for pretrained model."""
  return tf.keras.applications.inception_v3.preprocess_input(images), labels


def apply_normalization(images, labels, norm):
  """Apply normalization to the image batch.

  Args:
    images: A tensor batch of images.
    labels: A tensor batch of labels.

  Return:
    A tuple with zero-mean normalized image batch and a label batch of the same
    size as the input.
  """
  if norm == "rescale":
    images *= 1.0 / 255.0
  elif norm == "subtract_mean":
    images -= tf.reduce_mean(images, axis=(1, 2), keepdims=True)
  return images, labels


@functools.lru_cache(maxsize=128)
def get_dataset(split,
                balanced=False,
                augment=False,
                transfer=False,
                norm="rescale"):
  """Retrieve a dataset with or without augmentation.

  Args:
    split: The split to retrieve the dataset from. One of {'train', 'val',
      'test'}.
    balance: Whether to balance the false and positive classes in each batch
      of data.
    augment: Whether to apply augmentation to the returned dataset.
    transfer: Whether to apply the pretrained model preprocessing. If true, norm
      is ignored.
    norm: The type of normalization to apply to every image in the
      batch. One of {"rescale", "subtract_mean"}.

  Returns:
    A tensorflow Dataset object that iterates over sample batches.
  """
  if balanced:
    pos_ds = get_dataset_for_label(split, "benign")
    neg_ds = get_dataset_for_label(split, "malignant")
    balanced_ds = tf.data.experimental.sample_from_datasets(
        datasets=[pos_ds, neg_ds], weights=[0.5, 0.5], seed=SEED)
    balanced_ds = balanced_ds.batch(BATCH_SIZE).repeat()
    balanced_ds.class_names = LABELS
    cur_dataset = balanced_ds
  else:
    cur_dataset = keras.preprocessing.image_dataset_from_directory(
        get_dataset_dir(split),
        seed=SEED,
        shuffle=(split == "train"),
        image_size=IMAGE_SIZE,
        batch_size=BATCH_SIZE)
  if augment:
    cur_dataset = cur_dataset.map(apply_augment)
  if transfer:
    cur_dataset = cur_dataset.map(inceptionv3_preprocess)
  else:
    cur_dataset = cur_dataset.map(lambda x, y: apply_normalization(x, y, norm))
  cur_dataset.class_names = LABELS
  return cur_dataset
