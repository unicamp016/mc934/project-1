import argparse
import itertools
import json
import shutil
import sys
import time

import numpy as np
import tensorflow as tf
import tensorflow.keras as keras
from tensorboard.plugins.hparams import api as hp

from dataset import *
from util import *


def make_pretrained_model(frozen):
  """Create InceptionV3-based model transfering the weights."""
  inception = keras.applications.InceptionV3(
      include_top=False,
      weights="imagenet",
      input_shape=(IMAGE_SIZE[0], IMAGE_SIZE[1], 3),
  )
  inception.trainable = not frozen
  model = keras.Sequential([
      inception,
      keras.layers.GlobalAveragePooling2D(name="global-average"),
      keras.layers.Dense(1, activation="sigmoid", name="dense-2")
  ])
  return model


def make_custom_model(hparams, output_bias="zeros"):
  layer_config = {
      "kernel_initializer": hparams["initializer"],
      "activation": hparams["activation"],
  }
  inputs = keras.Input(shape=(IMAGE_SIZE[0], IMAGE_SIZE[1], 3))
  conv_out = inputs
  for i in range(hparams["num_layers"]):
    conv_out = keras.layers.Conv2D(
        32, 3, name=f"conv2d-{i}", **layer_config)(
            conv_out)
    conv_out = keras.layers.MaxPooling2D(name=f"maxpool-{i}")(conv_out)
  flat = keras.layers.Flatten(name="flatten")(conv_out)
  dense1 = keras.layers.Dense(128, name="dense-1", **layer_config)(flat)
  if hparams["dropout"]:
    dense1 = keras.layers.Dropout(0.5, name="dropout-1")(dense1)
  pred = keras.layers.Dense(
      units=1,
      activation="sigmoid",
      bias_initializer=output_bias,
      name="dense-2")(
          dense1)
  custom_model = keras.Model(inputs=inputs, outputs=pred)
  return custom_model


def compile_model(model, verbose=False):
  """Return the compiled model with default optimizer, loss, and metrics."""
  model.compile(
      optimizer='adam',
      loss=tf.keras.losses.BinaryCrossentropy(from_logits=False),
      metrics=[
          keras.metrics.TruePositives(name='tp'),
          keras.metrics.FalsePositives(name='fp'),
          keras.metrics.TrueNegatives(name='tn'),
          keras.metrics.FalseNegatives(name='fn'),
          keras.metrics.BinaryAccuracy(name='accuracy'),
          keras.metrics.Precision(name='precision'),
          keras.metrics.Recall(name='recall'),
          keras.metrics.AUC(name="auc"),
      ])
  model.summary()
  return model


def fit_model(model, class_weights, steps_per_epoch, hparams, args, name):
  """Fit the model and save results."""
  train_ds = get_dataset(
      "train",
      balanced=args.balanced,
      augment=args.augment,
      transfer=args.transfer,
      norm=args.normalization)
  val_ds = get_dataset("val", transfer=args.transfer, norm=args.normalization)

  try:
    shutil.rmtree(get_log_dir(name), ignore_errors=True)
    shutil.rmtree(get_ckpt_dir(name), ignore_errors=True)
    os.makedirs(get_log_dir(name))
    os.makedirs(get_ckpt_dir(name))
    with open(os.path.join(get_log_dir(name), "config.json"), "w") as cf:
      config = vars(args)
      config.update(hparams)
      cf.write(json.dumps(config, indent=2))
      model.summary(print_fn=lambda summary: cf.write(f"\n\n{summary}\n"))
    train_config = get_train_config(hparams, name)
    start_time = time.time()
    history = model.fit(
        x=train_ds,
        validation_data=val_ds,
        class_weight=class_weights,
        steps_per_epoch=steps_per_epoch,
        **train_config)
    np.save(os.path.join(get_log_dir(name), "history.npy"), history.history)
  except KeyboardInterrupt:
    sys.exit(0)
  finally:
    with open(os.path.join(get_log_dir(name), "time.txt"), "w") as f:
      time_s = time.time() - start_time
      f.write(f"Time to train: {time_s:.2f}s\n")
    model.save(os.path.join(get_ckpt_dir(name), "last_model"))


def main(args):
  # Create special initializer for output bias.
  output_bias = 0
  if args.initial_bias:
    output_bias = np.log(POS / NEG)
  output_bias = keras.initializers.Constant(output_bias)

  # Set class weights.
  class_weights = None
  if args.class_weights:
    class_weights = {
        0: TOTAL / NEG,
        1: TOTAL / POS,
    }

  # Specify steps per epoch for the balanced dataset
  steps_per_epoch = None
  if args.balanced:
    steps_per_epoch = np.ceil(2 * NEG / BATCH_SIZE)

  if args.compare_hparams:
    for param_values in itertools.product(
        *tuple(map(lambda p: p.domain.values, HP_PARAMS))):
      exp_name = ""
      hparams_dict = {}
      for hparam, value in zip(HP_PARAMS, param_values):
        hparams_dict[hparam.name] = value
        if hparam.name == "dropout":
          if value:
            exp_name += f"_with_dropout"
          else:
            exp_name += f"_no_dropout"
        else:
          exp_name += f"_{value}"
      name = os.path.join(args.name, f"{args.name}{exp_name}")
      custom_model = make_custom_model(hparams_dict, output_bias=output_bias)
      custom_model = compile_model(custom_model)
      fit_model(custom_model, class_weights, steps_per_epoch, hparams_dict,
                args, name)
  elif args.transfer:
    hparams_dict = {}
    pretrained_model = make_pretrained_model(args.frozen)
    pretrained_model = compile_model(pretrained_model)
    fit_model(pretrained_model, class_weights, steps_per_epoch, hparams_dict,
              args, args.name)
  else:
    hparams_dict = {
        "num_layers": 3,
        "activation": "relu",
        "initializer": "glorot_uniform",
        "dropout": False,
    }
    custom_model = make_custom_model(hparams_dict, output_bias=output_bias)
    custom_model = compile_model(custom_model)
    fit_model(custom_model, class_weights, steps_per_epoch, hparams_dict, args,
              args.name)


def train_model(name="baseline",
                initial_bias=False,
                class_weights=False,
                normalization="rescale",
                balanced=False,
                compare_hparams=False,
                augment=False,
                transfer=False,
                frozen=False):
  """Wrapper function that calls `main` with a Namespace object."""
  args = argparse.Namespace()
  args.name = name
  args.initial_bias = initial_bias
  args.class_weights = class_weights
  args.normalization = normalization
  args.balanced = balanced
  args.compare_hparams = compare_hparams
  args.augment = augment
  args.transfer = transfer
  args.frozen = frozen
  gpus = tf.config.experimental.list_physical_devices('GPU')
  tf.config.experimental.set_memory_growth(gpus[0], True)
  main(args)


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-n",
      "--name",
      type=str,
      default="compare",
      help="Name of the directory to save logs and model to.")
  parser.add_argument(
      "--initial_bias",
      action="store_true",
      help="Whether to initialize the bias of the output layer considering the "
      "label distribution.")
  parser.add_argument(
      "--class_weights",
      action="store_true",
      help="Whether to apply weights proportional to class distribution to the "
      "loss.")
  parser.add_argument(
      "--normalization",
      default="rescale",
      choices=["rescale", "subtract_mean"],
      help="Normalization type to apply to images before feeding to dataset.")
  parser.add_argument(
      "--balanced",
      action="store_true",
      help="Whether to balance the labels in each training batch.")
  parser.add_argument(
      "--compare_hparams",
      action="store_true",
      help="Whether to run the experiment that compares hyperparameters.")
  parser.add_argument(
      "--augment",
      action="store_true",
      help="Whether to apply data augmentation.")
  parser.add_argument(
      "--transfer",
      action="store_true",
      help="Whether to use transfer learning and start from a pretrained "
      "model.")
  parser.add_argument(
      "--frozen",
      action="store_true",
      help="Whether to freeze or not the convolutiona layers of the pretrained "
      "when transfer learning. Only does something if transfer is True.")
  args = parser.parse_args()
  gpus = tf.config.experimental.list_physical_devices('GPU')
  tf.config.experimental.set_memory_growth(gpus[0], True)
  main(args)
