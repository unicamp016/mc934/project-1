import matplotlib.pyplot as plt
import numpy as np
import PIL

import seaborn as sns
import sklearn.metrics
from dataset import *
from util import *


def visualize_dataset_splits():
  """Print the number of samples per split and label percentage composition."""
  print("Number of samples for <split>-<label>:")
  for split in SPLITS:
    split_samples = []
    for label in LABELS:
      split_samples.append((f"{split}-{label}", len(get_samples(split, label))))
    total_samples = sum([label_sample[1] for label_sample in split_samples])
    for id, samples in split_samples:
      print(f"  {id}: {samples} ({samples * 100 / total_samples:.2f}%)")

  benign_sample = PIL.Image.open(get_samples("train", "benign")[0])
  malign_sample = PIL.Image.open(get_samples("train", "malignant")[0])
  fig, axes = plt.subplots(1, 2)
  axes[0].set_axis_off()
  axes[1].set_axis_off()
  axes[0].set_title("Benign")
  axes[0].imshow(benign_sample)
  axes[1].set_title("Malign")
  axes[1].imshow(malign_sample)
  fig.show()

  print(f"Benign sample image shape: {benign_sample.size}")
  print(f"Malign sample image shape: {malign_sample.size}")


def visualize_dataset(
    axes_shape=(3, 3), balanced=False, augment=False, norm="rescale"):
  train_ds = get_dataset("train", balanced=balanced, augment=augment, norm=norm)
  class_names = train_ds.class_names
  plt.figure(figsize=(10, 10))
  images_per_show = axes_shape[0] * axes_shape[1]
  for images, labels in train_ds.take(1):
    batch_size = len(images)
    print(f"Batch size: {batch_size}")
    for label_id, class_name in enumerate(class_names):
      nr_labels = np.count_nonzero(labels == label_id)
      label_perc = nr_labels * 100 / batch_size
      print(
          f"{class_name} ({label_id}) labels: {nr_labels} ({label_perc:.2f}%)")
    nr_shows = batch_size // images_per_show
    nr_shows += bool(batch_size % images_per_show)
    for b in range(nr_shows):
      for ax_id, img_id in enumerate(
          range(b * images_per_show, min((b + 1) * images_per_show,
                                         len(images)))):
        ax = plt.subplot(axes_shape[0], axes_shape[1], ax_id + 1)
        img = images[img_id].numpy()
        if norm == "rescale":
          img *= 255
        plt.imshow(img.astype(np.uint8))
        plt.title(class_names[labels[img_id]])
        plt.axis("off")
      press = plt.waitforbuttonpress()
      plt.close("all")
      if not press:
        break


def plot_metrics_impl(model_names, histories):
  metrics = ['loss', 'auc', 'precision', 'recall']

  common_prefix = os.path.commonprefix(model_names)

  fig, axes = plt.subplots(2, 2, figsize=(12, 8), dpi=150)

  title = os.path.basename(common_prefix)

  if not title == '':
    fig.suptitle(title.replace('_', ' ').capitalize())

  for model_name, history in zip(model_names, histories):
    epoch = range(1, len(history['loss']) + 1)
    for n, metric in enumerate(metrics):
      r_ax = n // 2
      c_ax = n % 2
      ax = axes[r_ax][c_ax]

      train_leg = f'{model_name.replace(common_prefix, "")}_train'

      ax.plot(epoch, history[metric], label=train_leg)

      val_leg = f'{model_name.replace(common_prefix, "")}_val'
      ax.plot(
        epoch,
        history['val_' + metric],
        linestyle="--",
        label=f'{val_leg}')

      name = metric.replace("_", " ").capitalize()
      ax.set_xlabel('Epoch')
      ax.set_ylabel(name)

      if r_ax == 0 and c_ax == 0:
        ax.legend()


  plt.show()

def plot_cm_impl(history, epoch=None):
  if not epoch:
    epoch = len(history['val_tp'])

  print(len(history['val_tp']))
  h = lambda k: history[k][epoch - 1]

  cm = np.array([[h('val_tn'), h('val_fp')],
                 [h('val_fn'), h('val_tp')]]).astype('int32')

  plt.figure(figsize=(5, 5), dpi=100)
  sns.heatmap(cm, annot=True, fmt="d")
  plt.title('Confusion matrix')
  plt.ylabel('Actual label')
  plt.xlabel('Predicted label')
  plt.show()


def plot_roc_impl(ax, name, model, val_ds):
  labels = [y for x, y in val_ds.unbatch()]
  predictions = model.predict(val_ds)

  fpr, tpr, _ = sklearn.metrics.roc_curve(labels, predictions)
  auc = sklearn.metrics.auc(fpr, tpr)

  ax.plot(100 * fpr, 100 * tpr, label=f'{name}, AUC = {auc:.3f}', linewidth=2)
  ax.set(xlabel='False positives [%]', ylabel='True positives [%]')


def plot_rocs_impl(names, models, val_dss):
  fig, ax = plt.subplots(1, 1, figsize=(5, 5), dpi=100)

  ax.plot(range(0, 100), linewidth=2, linestyle='dashed')

  for name, model, val_ds in zip(names, models, val_dss):
    plot_roc_impl(ax, name, model, val_ds)

  ax.grid(True)
  ax.set_aspect('equal')
  ax.legend()
  plt.show()


def plot_metrics(model_names):
  plot_metrics_impl(model_names, [ get_history(m) for m in model_names ])


def plot_cm(model_name, epoch=None):
  history = get_history(model_name)
  plot_cm_impl(history, epoch)


def plot_rocs(model_names):
  models = [get_best_model(m) for m in model_names]

  configs = [get_config_json(m) for m in model_names]

  val_dss = []

  for c in configs:
    val_dss.append(
        get_dataset('val', transfer=c['transfer'], norm=c['normalization']))

  plot_rocs_impl(model_names, models, val_dss)
  plt.show()
